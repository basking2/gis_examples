package com.gitlab.basking2;


import org.geotools.referencing.CRS;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.geotools.referencing.crs.DefaultProjectedCRS;
import org.geotools.referencing.operation.DefaultMathTransformFactory;
import org.geotools.referencing.operation.projection.MapProjection;
import org.geotools.referencing.operation.transform.AffineTransform2D;
import org.opengis.geometry.Geometry;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.crs.ProjectedCRS;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

import java.awt.geom.AffineTransform;

public class Example1 {
    public static void main(final String[] args) throws FactoryException, TransformException {
        final int lod = 7;
        final int cellWidth = 256;
        final int cellHeight = 256;
        final int xLength = cellWidth * (int) Math.pow(2, lod);
        final int yLength = cellHeight * (int) Math.pow(2, lod);

        final CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:4326");
        final MathTransform data2world = CRS.findMathTransform(sourceCRS, DefaultGeographicCRS.WGS84);
        final DefaultMathTransformFactory defaultMathTransformFactory = new DefaultMathTransformFactory();

        // Lat, Lon axis order.
        System.out.println("source min " + sourceCRS.getCoordinateSystem().getAxis(0).getMinimumValue());
        System.out.println("source min " + sourceCRS.getCoordinateSystem().getAxis(1).getMinimumValue());
        System.out.println("source max " + sourceCRS.getCoordinateSystem().getAxis(0).getMaximumValue());
        System.out.println("source max " + sourceCRS.getCoordinateSystem().getAxis(1).getMaximumValue());

        // Lan, Lat axis order.
        System.out.println("data2world min " + DefaultGeographicCRS.WGS84.getCoordinateSystem().getAxis(0).getMinimumValue());
        System.out.println("data2world min " + DefaultGeographicCRS.WGS84.getCoordinateSystem().getAxis(1).getMinimumValue());
        System.out.println("data2world max " + DefaultGeographicCRS.WGS84.getCoordinateSystem().getAxis(0).getMaximumValue());
        System.out.println("data2world max " + DefaultGeographicCRS.WGS84.getCoordinateSystem().getAxis(1).getMaximumValue());

        System.out.println("X len "+xLength+" Y len " + yLength);

        final AffineTransform2D world2screen = new AffineTransform2D(
                xLength / 360d,
                0d,
                0d,
                -yLength / 180d,
                xLength / 2,
                yLength / 2
        ); // start with identity transform

        final MathTransform transform = defaultMathTransformFactory.createConcatenatedTransform(data2world, world2screen);
        // final MathTransform transform = world2screen;

        // Some sample lat, lon pairs to convert.
        final double[] in = {
                90, -180,
                0, 0 ,
                0, 180,
                -90, 180};
        final double[] out = new double[in.length];

        transform.transform(in, 0, out, 0, in.length / 2); // / transform.getSourceDimensions());

        for (int i = 0; i < in.length; i+=2) {
            // Axis are exchanged.
            System.out.println(i + " x=" +in[i] + " -> "+ out[i+1]);
            System.out.println(i + " y=" +in[i+1] + " -> "+ out[i]);
        }
    }
}
