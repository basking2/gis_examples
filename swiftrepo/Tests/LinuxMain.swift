import XCTest

import swiftgeoTests

var tests = [XCTestCaseEntry]()
tests += swiftgeoTests.allTests()
XCTMain(tests)
