import XCTest

import GdalTests

var tests = [XCTestCaseEntry]()
tests += GdalTests.allTests()
XCTMain(tests)
