// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

#if os(macOS) || os(iOS) || os(tvOS) || os(watchOS)
import func Darwin.popen
#elseif os(Linux)
import func Glibc.popen
#endif

import Foundation

enum ShError: Error {
  case err(message: String)
}

@discardableResult
func shell(_ args: String...) -> (Int32, String, String) {
  let task = Process()
  task.executableURL = URL(string:"file:/usr/bin/env")
  task.arguments = args
  let stdout = Pipe()
  let stderr = Pipe()
  task.standardOutput = stdout
  task.standardError = stderr
  do {
    try task.run()
  } catch {
    print(error)
  }

  task.waitUntilExit()
  let data = stdout.fileHandleForReading.readDataToEndOfFile()
  let err = stderr.fileHandleForReading.readDataToEndOfFile()

  return (
    task.terminationStatus,
    String(decoding: data, as: UTF8.self),
    String(decoding: err, as: UTF8.self)
  )
}

var cflags: [String] = []
switch shell("gdal-config", "--cflags") {
  case (0, let stdout, _):
    cflags.append(stdout)
  case (_, _, let stderr):
    throw ShError.err(message: "Could not find gdal-config: \(stderr)")
}

var ldflags: [String] = []
switch shell("gdal-config", "--cflags") {
  case (0, let stdout, _):
    ldflags.append(stdout)
  case (_, _, let stderr):
    throw ShError.err(message: "Could not find gdal-config: \(stderr)")
}

let package = Package(
    name: "Gdal",
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "Gdal",
            targets: ["Gdal"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "Gdal",
            dependencies: [],
            cSettings: [
               .unsafeFlags(cflags)
            ]),
        .testTarget(
            name: "GdalTests",
            dependencies: ["Gdal"]),
    ]
)
