// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "swiftgeo",
    platforms: [
      .macOS(.v10_15),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/vapor/vapor", from: "4.5.0"),
        // .package(path: "./Cgdal"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .systemLibrary(
            name: "Cgdal",
            pkgConfig: "gdal"),
        .target(
            name: "swiftgeo",
            dependencies: ["Cgdal", .product(name: "Vapor", package: "vapor")]),
        .testTarget(
            name: "swiftgeoTests",
            dependencies: ["swiftgeo"]),
    ]
)
