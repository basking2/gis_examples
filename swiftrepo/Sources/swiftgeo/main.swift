import Cgdal
import Vapor

print("Hello, world!")

let a = Cgdal.GDALVersionInfo("VERSION_NUM")
print(String(cString: a!))


// ==== Vapor app. ====
let app = Application()

app.get("healthcheck") { req -> Response in
  return Response(
    status: .ok,
    version: .init(major: 1, minor: 1),
    headers: .init([("Content-Type", "application/json")]),
    body: "{ \"status\": \"OK!\"}"
  )
}

app.get() { req -> Response in
  return Response(
    status: .ok,
    version: .init(major: 1, minor: 1),
    headers: .init([("Content-Type", "application/json")]),
    body: "{ \"status\": \"OK other!\"}"
  )
}


try app.run()

// ==== End Vapor app. ====
