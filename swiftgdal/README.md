# swiftgdal

## Usage 

    swift run swiftgdal ~/Downloads/gwes00.glo_30m.t00z.grib2
    [3/3] Linking swiftgdal
    Processing file /home/sam/Downloads/gwes00.glo_30m.t00z.grib2
    Size is 720 x 321 x 1539
    Projection is `GEOGCS["Coordinate System imported from GRIB file",DATUM["unnamed",SPHEROID["Sphere",6371229,0]],PRIMEM["Greenwich",0],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AXIS["Latitude",NORTH],AXIS["Longitude",EAST]]'
    Origin = (-0.250000, 80.250000)
    Pixel Size = (0.500000, -0.500000)
