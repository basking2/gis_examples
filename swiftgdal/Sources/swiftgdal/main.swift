
import Cgdal
import Foundation

GDALAllRegister();

CommandLine.arguments.forEach { v in


    if v != CommandLine.arguments[0] {
        print("Processing file \(v)")

        // GDALDatasetH  hDataset;
        let hDataset = GDALOpen(v, GA_ReadOnly);

        print("Size is \(GDALGetRasterXSize(hDataset)) x \(GDALGetRasterYSize(hDataset)) x \(GDALGetRasterCount(hDataset))")

        let adfGeoTransform = UnsafeMutablePointer<Double>.allocate(capacity: 6)

        if hDataset != nil
        {
            if GDALGetProjectionRef(hDataset) != nil {
                print( "Projection is `\(String(cString: GDALGetProjectionRef(hDataset)))'")
            }
            if GDALGetGeoTransform( hDataset, adfGeoTransform ) == CE_None {
                print("Origin = (\(String(format: "%.6f", adfGeoTransform[0])), \(String(format: "%.6f", adfGeoTransform[3])))")
                print("Pixel Size = (\(String(format: "%.6f", adfGeoTransform[1])), \(String(format: "%.6f", adfGeoTransform[5])))")
            }

        }

        adfGeoTransform.deallocate()
        GDALClose(hDataset)
    }
}
