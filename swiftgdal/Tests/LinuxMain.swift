import XCTest

import swiftgdalTests

var tests = [XCTestCaseEntry]()
tests += swiftgdalTests.allTests()
XCTMain(tests)
