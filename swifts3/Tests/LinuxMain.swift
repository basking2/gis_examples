import XCTest

import swifts3Tests

var tests = [XCTestCaseEntry]()
tests += swifts3Tests.allTests()
XCTMain(tests)
