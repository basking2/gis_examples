import XCTest

import S3Tests

var tests = [XCTestCaseEntry]()
tests += S3Tests.allTests()
XCTMain(tests)
