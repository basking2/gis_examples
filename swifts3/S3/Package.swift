// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "S3",
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "S3",
            targets: ["S3"]),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "S3",
            dependencies: ["AwsCppSdkS3"]),
        .systemLibrary(
            name: "AwsCppSdkS3",
            pkgConfig: "aws-cpp-sdk-s3"),
        .testTarget(
            name: "S3Tests",
            dependencies: ["S3"]),
    ],
    cxxLanguageStandard: CXXLanguageStandard.cxx11
)
