#include "include/S3.h"

#include <aws/core/Aws.h>
#include <aws/s3/S3Client.h>
#include <aws/s3/model/Bucket.h>
#include <aws/s3/model/GetObjectRequest.h>

#include <iostream>

struct s3_client {
    Aws::S3::S3Client client;
};


struct s3_client* s3_client_create() {
    Aws::SDKOptions options;
    Aws::InitAPI(options);
    // s->options.loggingOptions.logLevel = Aws::Utils::Logging::LogLevel::Info;

    struct s3_client *s  = new s3_client();

    s->client = Aws::S3::S3Client();

    return s;
}

void s3_client_list_buckets(struct s3_client* s){
    auto outcome = s->client.ListBuckets();

    Aws::Vector<Aws::S3::Model::Bucket> bucket_list = outcome.GetResult().GetBuckets();

    for (auto const &bucket : bucket_list)
    {
        std::cout << "  * " << bucket.GetName() << std::endl;
    }

}

void s3_client_get_object(struct s3_client* client, const char* bucket, const char* key) {
    Aws::S3::Model::GetObjectRequest req;
    req.SetBucket(bucket);
    req.SetKey(key);
    auto resp = client->client.GetObject(req);

    const int n = 1024;
    int sz = n;
    int fill = 0;
    void* buffer = malloc(n);

    Aws::IOStream& io = resp.GetResult().GetBody();

    while (!io.read(static_cast<char*>(buffer)+fill, n).failbit) {
        fill += io.gcount();
        sz += n;
        buffer = realloc(buffer, sz);
        if (buffer == nullptr) {
            // Return error.
        }
    }

    if (io.eofbit) {
        // Return result.
    }
    else {
        // Report error.
    }

    free(buffer);
}

void s3_client_destroy(struct s3_client* s) {
    // Aws::ShutdownAPI(s->options);
    delete s;
}