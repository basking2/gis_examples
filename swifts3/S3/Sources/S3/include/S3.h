#ifndef S3_h
#define S3_h

#ifdef __cplusplus
extern "C" {
#endif

struct s3_client;

struct s3_client* s3_client_create();

void s3_client_list_buckets(struct s3_client*);

void s3_client_destroy(struct s3_client*);

#ifdef __cplusplus
}
#endif

#endif // S3_h