package main

import (
	"os"

	"gitlab.com/basking2/gis_examples/gogdal.v1/pkg/gogdal"
)

func main() {

	var g = gogdal.Gdal{}

	for i := 1; i < len(os.Args); i++ {
		println(g.DescFile(os.Args[i]))
	}

	if len(os.Args) < 2 {
		println("Usage: " + os.Args[0] + " [grib files]")
	}

}
