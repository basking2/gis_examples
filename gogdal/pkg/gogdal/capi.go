package gogdal

// #cgo pkg-config: gdal
// #include <gdal.h>
import "C"

import (
	"fmt"
)

type Gdal struct {
}

func (g *Gdal) ToString() string {
	return "Gdal ToString"
}

func (g *Gdal) DescFile(filename string) string {
	C.GDALAllRegister()

	var s = ""

	var hDataset = C.GDALOpen(C.CString(filename), C.GA_ReadOnly)

	s += fmt.Sprintf(
		"Size is %f x %f x %f.\n",
		float32(C.GDALGetRasterXSize(hDataset)),
		float32(C.GDALGetRasterYSize(hDataset)),
		float32(C.GDALGetRasterCount(hDataset)))

	if C.GDALGetProjectionRef(hDataset) != nil {
		var ref = C.GoString(C.GDALGetProjectionRef(hDataset))
		s += "Projection is " + ref + "\n"
	}

	var adfGeoTransform = [6]C.double{}

	if C.GDALGetGeoTransform(hDataset, &adfGeoTransform[0]) == C.CE_None {
		s += fmt.Sprintf("Origin = %.6f, %.6f\n", float64(adfGeoTransform[0]), float64(adfGeoTransform[3]))

		s += fmt.Sprintf("Pixel Size = (%.6f, %6f)\n", float64(adfGeoTransform[1]), float64(adfGeoTransform[5]))
	}

	return s
}
